﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RolloverEffect : MonoBehaviour
{
    public CanvasGroup canvasGroup; //canvas component on the panel we want to fade in and out (info panel)
    public Text textDisplay; //creates a component for ui object reference
    public float speed = 1f;

    bool fadeIn; 

    // Start is called before the first frame update
    void Start()
    {
    fadeIn = false;

    }

    // Update is called once per frame
    public void Update()
    {
        if (fadeIn)
        {//if coming in
            canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, 1f, Time.deltaTime * speed); //go opaque 
        }
    else //if leaving
        {
            canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, 0f, Time.deltaTime * speed);
        }
    }

    public void Enter(string toDisplay)
    {
        //fade in the panel and set text to what the inventory item is.
        fadeIn = true;
        textDisplay.text = toDisplay;//set text to what passes it in
    }

    public void Exit()
    {
        fadeIn = false;

    }

}

