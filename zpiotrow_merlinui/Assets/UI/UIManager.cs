﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIManager : MonoBehaviour
{
    public Animator inventoryPanel;
    public Animator potionPanel;
    public Animator infoPanelPotions;
    public Text numberOfPotions;

    private int count;
    // Start is called before the first frame update
    void Start()
    {
        count = 0;
    }
    
    //when u click inventory button:
    public void OpenInventory()
    {
        
        inventoryPanel.SetBool("isHidden", false);
    }
    public void OpenPotions()
    {
        potionPanel.SetBool("isHidden", false);
        infoPanelPotions.SetBool("isHidden", false);
    }

    public void CloseInventory()
    {
        inventoryPanel.SetBool("isHidden", true);
    }

    public void ClosePotions()
    {
        potionPanel.SetBool("isHidden", true);
        infoPanelPotions.SetBool("isHidden", true);
    }
    
    public void AddPotions()
    {
        count = count + 1;
        numberOfPotions.text = count.ToString();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
